import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient) { }

  getAllUsers(){
    console.log("inside getAllUsers");
    return this.http.get("https://jsonplaceholder.typicode.com/users")
  }

  getUserDetails(id:string)
  {
    return this.http.get("https://jsonplaceholder.typicode.com/users/"+`${id}`);
  }

  getCompanyDetails(id:string){
    return this.http.get("https://jsonplaceholder.typicode.com/users/"+`${id}`);
  }
}
