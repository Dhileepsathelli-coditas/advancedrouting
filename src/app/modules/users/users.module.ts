import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UserHomeComponent } from './user-home/user-home.component';
import { PersonComponent } from './person/person/person.component';
import { CompanyComponent } from './company/company.component';
import { DisplayComponent } from './display/display.component';


@NgModule({
  declarations: [
    UserHomeComponent,
    PersonComponent,
    CompanyComponent,
    DisplayComponent
  ],
  imports: [
    CommonModule,
    UsersRoutingModule
  ]
})
export class UsersModule { }
