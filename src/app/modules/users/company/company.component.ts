import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserHomeComponent } from '../user-home/user-home.component';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnInit {

  constructor(private route: ActivatedRoute, private userhome: UserHomeComponent, private router: Router, private auth: AuthService) { }

  companyDetails: any;
  ngOnInit(): void {
    console.log(this.router.url);
    //can extract 1 
    const arr = this.router.url.split('/');
    let userId = arr[2];
    console.log(userId);

    this.auth.getCompanyDetails(userId).subscribe((response) => {
      console.log(response);
      this.companyDetails = response;
    })

  }
}
