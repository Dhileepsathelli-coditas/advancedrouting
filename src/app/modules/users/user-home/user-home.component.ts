import { Component, OnInit } from '@angular/core';
import { IUser } from 'src/app/types/interface';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.scss']
})
export class UserHomeComponent implements OnInit {

  users: IUser[] = [];
  error = "";

  constructor(private authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getAllUsers().subscribe({
      next: (response: any) => {
        this.users = response;
        console.log(this.users);
      },
      error: (error: any) => {
        this.error = error.message;
      },
      complete: () => {
        this.error = "complete"
      }
    });
  }

}
