import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CompanyComponent } from './company/company.component';
import { PersonComponent } from './person/person/person.component';
import { UserHomeComponent } from './user-home/user-home.component';
import { UserGuard } from 'src/app/guards/user.guard';
import { DisplayComponent } from './display/display.component';

const routes: Routes = [
  {
    path: '', component: UserHomeComponent,
    children: [
      {
        path: ':id', component: DisplayComponent,
        children: [
          // {path:'person',loadChildren:()=>import('./person/person/person.component').then(m=>m.PersonComponent),canActivate:[UserGuard]},
          // {path:'company',loadChildren:()=>import('./company/company.component').then(m=>m.CompanyComponent),canActivate:[UserGuard]},
          { path: 'person', component: PersonComponent },
          { path: 'company', component: CompanyComponent }

        ]
      }
    ],
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
