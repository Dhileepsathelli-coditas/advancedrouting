import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserHomeComponent } from '../../user-home/user-home.component';

@Component({
  selector: 'app-person',
  templateUrl: './person.component.html',
  styleUrls: ['./person.component.scss']
})
export class PersonComponent implements OnInit {

  constructor(private route: ActivatedRoute, private userhome: UserHomeComponent, private router: Router, private auth: AuthService) { }

  userDetails: any;
  ngOnInit(): void {
    console.log(this.router.url);
    //can extract 1 
    const splittedArr = this.router.url.split('/');
    let userId = splittedArr[2];
    console.log(userId);

    this.auth.getUserDetails(userId).subscribe((response) => {
      this.userDetails = response;
    })
  }
}