import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsComponent } from './details/details.component';
import { ProductHomeComponent } from './product-home/product-home.component';
import { StocksComponent } from './stocks/stocks.component';

const routes: Routes = [
  {
    path: '', component: ProductHomeComponent,
    children: [
      { path: 'details', component: DetailsComponent },
      { path: 'stocks', component: StocksComponent },
      { path: '', redirectTo: '/prodcts', pathMatch: 'full' }
    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProdctsRoutingModule { }
