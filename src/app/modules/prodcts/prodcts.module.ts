import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProdctsRoutingModule } from './prodcts-routing.module';
import { ProductHomeComponent } from './product-home/product-home.component';
import { DetailsComponent } from './details/details.component';
import { StocksComponent } from './stocks/stocks.component';




@NgModule({
  declarations: [
    ProductHomeComponent,
    DetailsComponent,
    StocksComponent
  ],
  imports: [
    CommonModule,
    ProdctsRoutingModule
  ]
})
export class ProdctsModule { }
