import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserGuard } from './guards/user.guard';

const routes: Routes = [
  //way of doing lazy loading path route
  // { path: '', redirectTo: '/login', pathMatch: 'full' },
  {path:'users',loadChildren:()=>import('./modules/users/users.module').then(m=>m.UsersModule),canActivate:[UserGuard]},
  {path:'prodcts',loadChildren:()=>import('./modules/prodcts/prodcts.module').then(m=>m.ProdctsModule),canActivate:[UserGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
