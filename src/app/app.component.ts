import { Component, OnInit } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'advancedRouting';
  users= [];
  error="";
  constructor(private authService:AuthService){}

  ngOnInit(): void {
    this.authService.getAllUsers().subscribe({
      next:(Response:any)=>{
        this.users=Response;
      },
      error:(error:any)=>{
        this.error=error.message;
      },
      complete:()=>{
        this.error="complete"
      }
    });
  }
}
